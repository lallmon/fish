set -gx PATH /usr/local/bin:/usr/local/sbin:$PATH
alias reload 'source ~/.config/fish/config.fish'

#these are bound to archlinux, annoying'
#set -Ux ARDUINO_CORE_PATH '/usr/share/arduino/hardware/archlinux-arduino/avr/cores/arduino'
#set -Ux ARDUINO_LIB_PATH '/usr/share/arduino/hardware/archlinux-arduino/libraries'
#set -Ux ARDUINO_VAR_PATH '/usr/share/arduino/hardware/archlinux-arduino/avr/variants'
#set -Ux BOARDS_TXT '/usr/share/arduino/hardware/archlinux-arduino/avr/boards.txt'

#arduino
alias ard 'arduino-cli'
#shell

alias list 'll -a'
alias emacs 'emacs &'

#git
alias gbda 'git branch --merged | grep -v \* | xargs git branch -D'
alias amend 'git commit --amend'
alias gcm 'git checkout master'
alias gco 'git checkout'
alias gcl 'git checkout -'
alias gcb 'git checkout -b'
alias gpf 'git push -f'
alias grm 'git rebase master'
alias gs  'git status'
alias gl  'git log'
alias gbd 'git branch -D'
alias gsearch 'git log --oneline | grep'
alias gpn "git push --set-upstream origin (git branch | grep \* | cut -d ' ' -f2-)"


#docker

alias fig 'docker-compose'
alias dorker 'docker'
alias dockre 'docker'
alias dockr 'docker'
alias rmia 'docker rmi (docker images -q) --force'
alias rma 'docker rm (docker ps -a -q)'


